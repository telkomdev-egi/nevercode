import { StyleSheet } from 'react-native';

import { COLOR_GREEN, COLOR_GREEN_LIGHT, COLOR_GREEN_STRONG } from '../../../styles';

const borderRadius = 20;
const font = {
  color: '#ffffff'
};

const button = {
  borderRadius,
  ...font,
  height: 40,
  lineHeight: 40,
  width: 200,
  textAlign: 'center',
  textAlignVertical: 'center',
  overflow: 'hidden'
};

export default StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    height: 40,
    width: 200,
    borderRadius
  },
  button: {
    ...button,
    backgroundColor: COLOR_GREEN
  },
  buttonPressed: {
    ...button,
    backgroundColor: COLOR_GREEN_STRONG
  },
  buttonDisabled: {
    ...button,
    backgroundColor: COLOR_GREEN_LIGHT
  }
});

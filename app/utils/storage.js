import { AsyncStorage } from 'react-native';

class Storage {
  constructor(storage = AsyncStorage) {
    this.storage = storage;
  }

  get = async key => {
    const value = await this.storage.getItem(key);
    if (!value) return {};
    return JSON.parse(value);
  };

  set = async (key, value) => {
    const jsonValue = JSON.stringify(value);
    await this.storage.setItem(key, jsonValue);
  };

  merge = async (key, value) => {
    const jsonValue = JSON.stringify(value);
    await this.storage.mergeItem(key, jsonValue);
  };

  remove = async key => {
    await this.storage.removeItem(key);
  };
}

export { Storage };
export default new Storage(AsyncStorage);

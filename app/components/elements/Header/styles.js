import { StyleSheet } from 'react-native';
import {
  COLOR_GREY_EXTRA_STRONG,
  COLOR_GREY,
  COLOR_YELLOW,
  FONT_TITLE,
  FONT_VALUE_MEDIUM
} from '../../../styles';

export default StyleSheet.create({
  container: {
    backgroundColor: COLOR_YELLOW
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: 8,
    paddingTop: 8,
    justifyContent: 'space-between'
  },
  textInputContainerStyle: {
    marginHorizontal: 16
  },
  containerTitle: {
    marginLeft: 22
  },
  title: {
    ...FONT_TITLE,
    color: COLOR_GREY_EXTRA_STRONG
  },
  subTitle: {
    ...FONT_VALUE_MEDIUM,
    color: COLOR_GREY
  }
});

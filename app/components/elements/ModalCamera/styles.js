import { StyleSheet } from 'react-native';
import {
  COLOR_WHITE,
  FONT_DISPLAY_REGULAR,
  COLOR_GREY_STRONG,
  COLOR_GREY_EXTRA_STRONG,
  COLOR_BLACK_OPACITY50,
  FONT_VALUE_MEDIUM
} from '../../../styles';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    zIndex: 1,
    backgroundColor: COLOR_BLACK_OPACITY50
  },
  modal: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: COLOR_WHITE,
    paddingTop: 15,
    paddingBottom: 30
  },
  title: {
    ...FONT_DISPLAY_REGULAR,
    paddingVertical: 10,
    textAlign: 'left',
    color: COLOR_GREY_STRONG,
    alignSelf: 'stretch',
    marginLeft: 20
  },
  menus: {
    ...FONT_VALUE_MEDIUM,
    textAlign: 'left',
    alignSelf: 'stretch',
    marginLeft: 20,
    color: COLOR_GREY_EXTRA_STRONG,
    alignItems: 'center',
    justifyContent: 'center'
  },
  selection: {
    paddingVertical: 12,
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 20
  },
  cancel: {
    paddingVertical: 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  img: {
    height: 25,
    width: 25,
    alignSelf: 'center'
  }
});

export default styles;

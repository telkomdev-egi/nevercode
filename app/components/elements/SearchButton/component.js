import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { IMAGES } from '../../../configs';

export default class Component extends React.Component {
  render() {
    const { imageStyle, onPress } = this.props;
    return (
      <TouchableOpacity style={styles.iconContainer} onPress={onPress}>
        <Image source={IMAGES.search} style={[styles.image, imageStyle]} />
      </TouchableOpacity>
    );
  }
}

Component.propTypes = {
  imageStyle: PropTypes.object,
  onPress: PropTypes.func.isRequired
};

Component.defaultProps = {
  imageStyle: {}
};

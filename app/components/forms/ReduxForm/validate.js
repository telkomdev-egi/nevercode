const validate = values => {
  const errors = {};
  if (!values.fname) {
    errors.fname = 'Please fill the fname';
  } else if (values.fname.length > 5) {
    errors.fname = 'we need first name max 5';
  }
  return errors;
};

export default validate;

export const PRIMARY_COLOR = '#1ea54f';

export const COLOR_GREEN = '#1ea54f';
export const COLOR_GREEN_OPACITY = 'rgba(30,165,79,0.5)';
export const COLOR_GREEN_STRONG = '#21b55a';
export const COLOR_GREEN_LIGHT = '#a5e0bc';

export const COLOR_GOLD_STRONG = '#e08b27';
export const COLOR_GOLD = '#ebac43';
export const COLOR_GOLD_LIGHT = '#f5d5a1';
export const COLOR_GOLD_EXTRA_LIGHT = '#f8d227';

export const COLOR_GREY_EXTRA_STRONG = '#666666';
export const COLOR_GREY_STRONG = '#8c8c8c';
export const COLOR_GREY = '#bdbdbd';
export const COLOR_GREY_OPACITY50 = 'rgba(219,219,219,0.5)';
export const COLOR_GREY_LIGHT = '#e4e7eb';
export const COLOR_DARK_GREY = '#797979';
export const COLOR_GREY_OPACITY80 = 'rgba(74,74,74, 0.8)';

export const COLOR_DARK = '#000000';
export const COLOR_LIGHT = '#f9fcff';
export const COLOR_EXTRA_LIGHT = '#ffffff';

export const COLOR_TRANSPARENT = 'rgba(0,0,0,0)';
export const COLOR_BLACK_OPACITY50 = 'rgba(0,0,0,0.5)';
export const COLOR_WHITE_OPACITY50 = 'rgba(255,255,255,0.5)';
export const COLOR_WHITE_OPACITY70 = 'rgba(255,255,255,0.7)';
export const COLOR_WHITE_OPACITY0 = 'rgba(255,255,255,0)';

export const COLOR_WHITE = '#ffffff';

export const COLOR_ERROR = '#d0021b';

export const COLOR_BLUE = '#47acf7';

export const COLOR_YELLOW = '#f8e71c';

export const COLOR_GREEN_APPROVED = '#58cd85';

export const FONT_H1 = {
  // fontFamily: 'quicksandbold',
  fontSize: 20
};

export const FONT_H1_MEDIUM = {
  // fontFamily: 'quicksandmedium',
  fontSize: 20
};

export const FONT_H2 = {
  //  fontFamily: 'quicksandbold',
  fontSize: 16
};

export const FONT_DISPLAY_BOLD = {
  //  fontFamily: 'quicksandbold',
  fontSize: 14
};

export const FONT_DISPLAY_REGULAR = {
  //  fontFamily: 'quicksandregular',
  fontSize: 14
};

export const FONT_LABEL_BOLD = {
  //  fontFamily: 'quicksandbold',
  fontSize: 12
};

export const FONT_TAG_LABEL_BOLD = {
  //  fontFamily: 'quicksandbold',
  fontSize: 10
};

export const FONT_DISPLAY_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 14
};

export const FONT_ERROR_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 10
};

export const FONT_LABEL_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 12
};

export const FONT_VALUE_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 16
};

export const FONT_LABEL_REGULAR = {
  //  fontFamily: 'quicksandregular',
  fontSize: 12
};

export const FONT_ERROR = {
  //  fontFamily: 'quicksandregular',
  fontSize: 12,
  color: COLOR_ERROR
};
export const FONT_ERROR_TITLE = {
  //  fontFamily: 'quicksandregular',
  fontSize: 14,
  color: COLOR_ERROR
};

export const FONT_DROPDOWN_REGULAR = {
  //  fontFamily: 'quicksandregular',
  fontSize: 16
};

export const FONT_TITLE = {
  //  fontFamily: 'quicksandbold',
  fontSize: 24
};

export const FONT_TITLE_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 24
};

export const FONT_SUPERSCRIPT_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 9
};

export const FONT_WARNING_MEDIUM = {
  //  fontFamily: 'quicksandmedium',
  fontSize: 16
};

import { StyleSheet } from 'react-native';
import { COLOR_GREY_STRONG } from '../../../styles';

export default StyleSheet.create({
  iconContainer: {
    width: 44,
    height: 44,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    tintColor: COLOR_GREY_STRONG,
    width: 32,
    height: 32
  }
});

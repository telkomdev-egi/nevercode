import React from 'react';
import { Image, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: {
        notifCount: 0
      }
    };
  }

  render() {
    const { iconStyle, iconSource, isBadge } = this.props;
    const { notification } = this.state;
    const { notifCount } = notification;
    const img = iconSource || null;
    return (
      <View>
        <View style={styles.mainView}>
          <Image source={img} style={[styles.icon, iconStyle]} resizeMode="contain" />
        </View>
        {isBadge &&
          notifCount > 0 && (
            <View style={styles.IconBadge}>
              <Text style={styles.text}>{notifCount}</Text>
            </View>
          )}
      </View>
    );
  }
}

Component.propTypes = {
  iconStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  iconSource: PropTypes.oneOfType([PropTypes.object, PropTypes.number]).isRequired,
  isBadge: PropTypes.bool
};

Component.defaultProps = {
  iconStyle: {},
  isBadge: false
};

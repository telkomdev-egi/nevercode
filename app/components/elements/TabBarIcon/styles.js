import { StyleSheet } from 'react-native';
import { COLOR_WHITE, COLOR_ERROR } from '../../../styles';

export default StyleSheet.create({
  icon: {},
  IconBadge: {
    position: 'absolute',
    top: 1,
    right: 1,
    width: 18,
    height: 18,
    borderRadius: 9,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLOR_ERROR
  },
  mainView: {
    height: 36,
    width: 36,
    backgroundColor: COLOR_WHITE,
    margin: 3
  },
  text: {
    color: COLOR_WHITE,
    fontSize: 10
  }
});

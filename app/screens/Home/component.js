import React from 'react';
import { View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import Form from '../../components/forms/Form';
import MainScreen from '../../components/layouts/MainScreen';
import { IMAGES } from '../../configs';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      dateOfbirth: '',
      provinces: [
        { id: '1', name: 'DKI JAKARTA' },
        { id: '2', name: 'JAWA BARAT' },
        { id: '3', name: 'JAWA TENGAH' },
        { id: '4', name: 'JAWA TIMUR' },
        { id: '5', name: 'DIY' }
      ],
      genderOptions: [
        {
          name: 'Male',
          id: '1'
        },
        {
          name: 'Femalekuku',
          id: '2'
        },
        {
          name: 'Dll',
          id: '3'
        }
      ],
      provinceValue: 'DIY',
      radioButtonValue: 'Male',
      errorTextUserName: 'error'
    };
  }

  async componentDidMount() {
    const { actions } = this.props;
    await actions.fetchGetListUser();
  }

  _validate = text => text;

  _onPress = () => {
    const { navigation } = this.props;
    navigation.navigate('Home');
  };

  _onFormChange = async (key, value) => {
    await this.setState({ [key]: value });
    await this.setState({ errorTextUserName: '', isModal: !this.state.isModal });
  };

  _onCloseModal = () => {
    this.setState({
      isModal: !this.state.isModal
    });
  };

  render() {
    const {
      userName,
      password,
      dateOfbirth,
      provinces,
      provinceValue,
      genderOptions,
      radioButtonValue,
      errorTextUserName
    } = this.state;
    const { navigation } = this.props;
    const data = [
      {
        label: 'Username',
        placeholder: 'Username',
        defaultValue: userName,
        type: 'TextInput',
        key: 'userName',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        },
        validate: this._validate,
        errorText: errorTextUserName
      },
      {
        label: 'Password',
        placeholder: 'Password',
        defaultValue: password,
        type: 'TextInput',
        key: 'password',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        }
      },
      {
        label: 'Date of Birth',
        placeholder: 'dd/mm/yyyy',
        type: 'Picker',
        defaultValue: dateOfbirth,
        key: 'dateOfbirth'
      },
      {
        label: 'Desa',
        placeholder: 'Pilih Desa',
        type: 'DropDown',
        options: provinces,
        defaultValue: provinceValue,
        key: 'provinceValue'
      },
      {
        label: 'Gender',
        type: 'RadioButton',
        options: genderOptions,
        defaultValue: radioButtonValue,
        key: 'radioButtonValue'
      },
      {
        label: 'Desa',
        placeholder: 'Pilih Desa',
        type: 'DropDown',
        options: provinces,
        defaultValue: provinceValue,
        key: 'provinceValue'
      },
      {
        label: 'Desa',
        placeholder: 'Pilih Desa',
        type: 'DropDown',
        options: provinces,
        defaultValue: provinceValue,
        key: 'provinceValue'
      },
      {
        label: 'Username',
        placeholder: 'Username',
        defaultValue: userName,
        type: 'TextInput',
        key: 'userName',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        },
        validate: this._validate,
        errorText: errorTextUserName
      },
      {
        label: 'Username',
        placeholder: 'Username',
        defaultValue: userName,
        type: 'TextInput',
        key: 'userName',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        },
        validate: this._validate,
        errorText: errorTextUserName
      },
      {
        label: 'Username',
        placeholder: 'Username',
        defaultValue: userName,
        type: 'TextInput',
        key: 'userName',
        images: {
          on: IMAGES.userOn,
          off: IMAGES.userOff
        },
        validate: this._validate,
        errorText: errorTextUserName
      }
    ];

    return (
      <MainScreen navigation={navigation} title="Home" showSearchButton>
        <ScrollView>
          <View style={styles.container}>
            <Form data={data} onFormChange={this._onFormChange} />
          </View>
        </ScrollView>
      </MainScreen>
    );
  }
}

Component.propTypes = {
  navigation: PropTypes.object.isRequired
};

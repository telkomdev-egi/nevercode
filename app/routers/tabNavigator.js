import React from 'react';
import { TabNavigator } from 'react-navigation';
import { View } from 'react-native';
import { HomeStack, DevelopmentStack } from './stackNavigator';
import TabBarBottom from '../components/elements/TabBarBottom';
import TabBarIcon from '../components/elements/TabBarIcon';
import { IMAGES } from '../configs';

const COLOR_DARK_GREY = '#797979';
const COLOR_GREEN = '#1ea54f';
const COLOR_GREY = '#bdbdbd';
const COLOR_TRANSPARENT = 'rgba(0,0,0,0)';
const COLOR_WHITE = '#ffffff';

const createTab = ({ stack, label, image, iconStyle, isBadge }) => ({
  screen: stack,
  navigationOptions: ({ navigation }) => ({
    tabBarLabel: label,
    tabBarIcon: ({ tintColor }) => {
      return (
        <View style={{ height: 32, width: 32, backgroundColor: '#fff' }}>
          <TabBarIcon
            iconSource={tintColor === COLOR_GREEN ? image.active : image.inactive}
            iconStyle={[iconStyle, { height: 30, width: 30, tintColor }]}
            isBadge={isBadge}
            navigation={navigation}
          />
        </View>
      );
    }
  })
});

const navigatorConfig = {
  tabBarComponent: TabBarBottom,
  tabBarPosition: 'bottom',
  backBehavior: true,
  lazy: true,
  swipeEnabled: false,
  animationEnabled: true,
  tabBarOptions: {
    showLabel: true,
    showIcon: true,
    activeTintColor: COLOR_GREEN,
    inactiveTintColor: COLOR_GREY,
    labelStyle: {
      color: COLOR_DARK_GREY
    },
    style: {
      justifyContent: 'space-between',
      backgroundColor: COLOR_WHITE,
      height: 72
    },
    tabStyle: {
      paddingTop: 12,
      paddingBottom: 12
    },
    indicatorStyle: {
      backgroundColor: COLOR_TRANSPARENT
    }
  }
};

const createTabNavigator = (tabDefinations = []) => {
  const tabs = tabDefinations.map(createTab);
  return new TabNavigator(tabs, navigatorConfig);
};

export const AppStack = createTabNavigator([
  {
    label: 'Home',
    stack: HomeStack,
    image: {
      active: IMAGES.userOn,
      inactive: IMAGES.userOff
    }
  },
  {
    label: 'Development',
    stack: DevelopmentStack,
    image: {
      active: IMAGES.passOn,
      inactive: IMAGES.passOff
    }
  }
]);

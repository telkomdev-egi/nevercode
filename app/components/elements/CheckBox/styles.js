import { StyleSheet } from 'react-native';

import { FONT_LABEL_MEDIUM, COLOR_GREY_EXTRA_STRONG } from '../../../styles';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  checkbox: {
    width: 16,
    height: 16
  },
  label: {
    marginLeft: 15,
    ...FONT_LABEL_MEDIUM,
    color: COLOR_GREY_EXTRA_STRONG
  }
});

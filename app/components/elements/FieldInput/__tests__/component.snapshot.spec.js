import { assertSnapshots } from '../../../../utils/TestUtils/snapshot';
import Component from '../component';

jest.mock('../styles.js', () => ({}));
jest.mock('../../TextInput', () => 'TextInput');
jest.mock('Text', () => 'Text');
jest.mock('View', () => 'View');
jest.mock('TouchableOpacity', () => 'TouchableOpacity');
jest.mock('FlatList', () => 'FlatList');
jest.mock('Image', () => 'Image');

describe('Component snapshot testing', () => {
  const configs = [
    {
      props: {},
      desc: 'renders with default style'
    },
    {
      state: {
        errorMessage: 'errorMessage'
      },
      props: {
        labelKey: 'save',
        containerStyle: {
          backgroundColor: '#333333'
        },
        labelStyle: {
          color: '#000000'
        },
        placeholderKey: 'placeholderKey',
        prefixInputText: 'Rp.',
        suffixInputText: 'suffixInputText'
      },
      desc: 'renders with props'
    },
    {
      state: {},
      props: {
        floatingLabel: true,
        defaultValue: ''
      },
      desc: 'renders with floatingLabel = true, defaultValue = "" '
    },
    {
      state: {},
      props: {
        floatingLabel: true,
        defaultValue: 'defaultValue'
      },
      desc: 'renders with floatingLabel = true, defaultValue.length > 0 '
    },
    {
      state: {
        status: 'focused'
      },
      props: {
        floatingLabel: true,
        prefixInputText: 'Rp.',
        defaultValue: ''
      },
      desc: 'renders with floatingLabel = true, defaultValue = "", prefixInputText: "Rp." '
    },
    {
      props: {
        onChangeInput: jest.fn()
      },
      desc: 'renders with onChangeInput'
    }
  ];
  assertSnapshots(Component, configs);
});

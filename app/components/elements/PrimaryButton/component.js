import React from 'react';
import { TouchableHighlight, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import { noop } from '../../../utils';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      status: 'normal'
    };
  }

  render() {
    const {
      disabled,
      title,
      onPress = noop,
      customContainerStyle,
      customButtonStyle,
      customButtonDisabledStyle
    } = this.props;
    const { status } = this.state;
    let buttonStyle = styles.button;

    if (status === 'pressed') buttonStyle = styles.buttonPressed;
    if (disabled) buttonStyle = styles.buttonDisabled;

    if (disabled) {
      return (
        <TouchableHighlight style={[styles.container, customContainerStyle]}>
          <Text style={[buttonStyle, customButtonDisabledStyle]}>{title}</Text>
        </TouchableHighlight>
      );
    }
    return (
      <TouchableHighlight
        style={[styles.container, customContainerStyle]}
        onShowUnderlay={() => this.setState({ status: 'pressed' })}
        onHideUnderlay={() => this.setState({ status: 'normal' })}
        onPress={onPress}
      >
        <Text style={[buttonStyle, customButtonStyle]}>{title}</Text>
      </TouchableHighlight>
    );
  }
}

Component.propTypes = {
  disabled: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  customButtonStyle: PropTypes.object,
  customContainerStyle: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  customButtonDisabledStyle: PropTypes.object
};

Component.defaultProps = {
  customButtonStyle: {},
  customContainerStyle: {},
  customButtonDisabledStyle: {}
};

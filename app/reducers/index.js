import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import home from '../screens/Home/reducer';

const rootReducer = combineReducers({
  form: formReducer,
  home
});

export default rootReducer;

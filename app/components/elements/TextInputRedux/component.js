import React from 'react';
import { View, Text, TextInput } from 'react-native';
import PropTypes from 'prop-types';
import { COLOR_ERROR } from '../../../styles';

export default class Component extends React.Component {
  render() {
    const {
      input: { onChange },
      label,
      meta: { error, touched }
    } = this.props;

    return (
      <View>
        <Text>{label}</Text>
        <TextInput
          style={{
            width: 200,
            borderWidth: 1,
            marginBottom: 10,
            padding: 0
          }}
          onChangeText={onChange}
        />
        {touched && (error && <Text style={{ color: COLOR_ERROR }}>{error}</Text>)}
      </View>
    );
  }
}

Component.propTypes = {
  input: {
    onChange: PropTypes.func.isRequired
  },
  label: PropTypes.string,
  meta: {
    error: PropTypes.string.isRequired,
    touched: PropTypes.bool.isRequired
  }
};

Component.defaultProps = {
  label: '',
  input: {},
  meta: {}
};

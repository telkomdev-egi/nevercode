import Component from '../index';
import { mockSetState } from '../../../../utils/TestUtils/snapshot';
import { noop } from '../../../../utils';

jest.mock('../styles.js', () => ({}));
jest.mock('../../TextInput', () => 'TextInput');
jest.mock('Text', () => 'Text');
jest.mock('View', () => 'View');
jest.mock('TouchableOpacity', () => 'TouchableOpacity');
jest.mock('FlatList', () => 'FlatList');
jest.mock('Image', () => 'Image');

jest.mock('../../../../utils', () => ({
  noop: jest.fn()
}));

describe('Component', () => {
  const ComponentWithMockedState = mockSetState(Component);
  const getInstance = props => new ComponentWithMockedState(props);
  beforeEach(() => {
    this.props = {
      onChangeInput: jest.fn(),
      floatingLabel: false,
      onBlur: jest.fn(),
      onFocus: jest.fn()
    };
  });

  afterEach(() => {
    this.props = null;
    jest.clearAllMocks();
  });

  test('_onTextInputChanged', () => {
    const instance = getInstance(this.props);
    const value = 1111;
    instance._onTextInputChanged(value);
    expect(this.props.onChangeInput).toHaveBeenCalledWith(value);
  });

  test('_onTextInputChanged onChangeInput undefined', () => {
    this.props.onChangeInput = undefined;
    const instance = getInstance(this.props);
    const value = 1111;
    instance._onTextInputChanged(value);
    expect(noop).toHaveBeenCalled();
  });

  test('when floatingLabel = true', () => {
    this.props.floatingLabel = true;
    const instance = getInstance(this.props);
    const value = 1111;
    instance._onTextInputChanged(value);
    expect(this.props.onChangeInput).toHaveBeenCalledWith(value);
  });

  test('when floatingLabel = true and value < 0', () => {
    this.props.floatingLabel = true;
    const instance = getInstance(this.props);
    const value = '';
    instance._onTextInputChanged(value);
    expect(this.props.onChangeInput).toHaveBeenCalledWith(value);
  });

  describe('_onValidation', () => {
    test('validate is defined', () => {
      const errorMessage = '123';
      const props = {
        ...this.props,
        validate: jest.fn(() => errorMessage)
      };
      const value = 'value';
      const instance = getInstance(props);
      instance._onValidation(value);
      expect(props.validate).toHaveBeenCalledWith(value);
      expect(instance.state.errorMessage).toBe(errorMessage);
    });

    test('validate is not defined', () => {
      const value = 'value';
      const instance = getInstance(this.props);
      instance._onValidation(value);
      expect(instance.state.errorMessage).toHaveLength(0);
    });
  });

  test('_onBlur', () => {
    const instance = getInstance(this.props);
    const text = 'text';
    instance._onBlur(text);
    expect(this.props.onBlur).toHaveBeenCalledWith(text);
  });

  test('_onBlur onBlur undefined', () => {
    this.props.onBlur = undefined;
    const instance = getInstance(this.props);
    const text = 'text';
    instance._onBlur(text);
    expect(noop).toHaveBeenCalled();
  });

  test('_onFocus', () => {
    const instance = getInstance(this.props);
    const text = 'text';
    instance._onFocus(text);
    expect(this.props.onFocus).toHaveBeenCalledWith(text);
  });

  test('_onFocus onFocus undefined', () => {
    this.props.onFocus = undefined;
    const instance = getInstance(this.props);
    const text = 'text';
    instance._onFocus(text);
    expect(noop).toHaveBeenCalled();
  });
});

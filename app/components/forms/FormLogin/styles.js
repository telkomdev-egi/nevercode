import { StyleSheet } from 'react-native';
import { PRIMARY_COLOR, COLOR_WHITE } from '../../../styles';

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: PRIMARY_COLOR
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: COLOR_WHITE
  },
  text: {
    fontSize: 20
  },
  radioContainer: {
    width: '100%',
    height: 24,
    marginTop: 8
  },
  image: {
    width: 100,
    height: 100,
    marginTop: 100,
    alignSelf: 'center'
  }
});

export default styles;
